# Software
# Python Packages
[<img src="images/python.png"  width="75" height="75" align="left">](https://www.python.org/)

Python is an interpreted high-level general-purpose programming language. Python is used for web development, AI, machine learning, operating systems, 
mobile application development, and video games. I contribute as developer/maintainer of the following Python packages:




```{note}
The original projects was developed by [Felipe Osorio](http://fosorios.mat.utfsm.cl/software.html). 
These repositories are only  a packages transform from R to Python.
```

### *fastmatrix*: Fast computation of some matrices useful in statistics
  
[![Gitlab Badge](https://img.shields.io/badge/fastmatrix-package-blue)](https://gitlab.com/FAAM/fastmatrix)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
![Progress](https://img.shields.io/badge/project-in%20progress-blue)
<!---
<img src="https://img.shields.io/pypi/dm/airflow?color=blue" alt="">
-->

Yet another R package for matrices. It contains a small set of functions to fast computation of some matrices and operations useful in statistics.

 

### *HEAVY*: Robust estimation using heavy-tailed distributions
[![Gitlab Badge](https://img.shields.io/badge/HEAVY-package-blue)](https://gitlab.com/FAAM)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
![Progress](https://img.shields.io/badge/project-paused-yellow)


Functions to perform robust estimation considering heavy-tailed distributions. Currently, the package includes linear regression, linear mixed-effect models, multivariate location and scatter estimation, multivariate regression, penalized splines and random variate generation.
 

### *L1pack*: Routines for L1 Estimation
[![Gitlab Badge](https://img.shields.io/badge/L1pack-package-blue)](https://gitlab.com/FAAM)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
![Progress](https://img.shields.io/badge/project-paused-yellow)

Provides routines to perform L1 estimation for linear regression, evaluation of density, distribution function, quantile function and random number generation for univariate and multivariate Laplace distribution.
 

### *MVT*: Estimation and testing for the multivariate t-distribution
[![Gitlab Badge](https://img.shields.io/badge/MVT-package-blue)](https://gitlab.com/FAAM)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
![Progress](https://img.shields.io/badge/project-paused-yellow)

This package contains a set of routines to perform estimation and inference under the multivariate t-distribution. These methods are a direct generalization of the multivariate inference under the gaussian assumption. In addition, these procedures provide robust methods useful against outliers.
 

### *SpatialPack*: Tools for assessment the association between two spatial processes
[![Gitlab Badge](https://img.shields.io/badge/SpatialPack-package-blue)](https://gitlab.com/FAAM)
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
![Progress](https://img.shields.io/badge/project-paused-yellow)


This package provides tools to assess the association between two spatial processes. Currently, four methodologies are implemented: A modified t-test to perform hypothesis testing about the independence between the processes, a suitable nonparametric correlation coefficient, the codispersion coefficient, and an F test for assessing the multiple correlation between one spatial process and several others. Functions for image processing and computing the spatial association between images are also provided. SpatialPack gives methods to complement methodologies that are available in geoR for one spatial process.


