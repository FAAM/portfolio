# Home

## Contact Information

[![Linkedin Badge](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/faam/) 
[![Github Badge](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/fralfaro) 
[![Gitlab Badge](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/FAAM) 

- Name: Francisco Antonio Alfaro Medina
- Phone: +56 9 94541139
- Email: francisco.alfaro.496@gmail.com
- Curriculum Vitae: <a href="https://gitlab.com/FAAM/curriculum_vitae/-/jobs/1363077500/artifacts/browse"><img alt="Link a la Documentación" src="https://img.shields.io/badge/CV-link-brightgreen"></a>





## About me 
🎮 Gaming | 🏀 Basketball | 💡 Learning | 📚 Teaching 

Statistician and Teacher at Universidad Técnica Federico Santa María.
I currently work as a Senior Data Scientist at ITAU (Bank). I enjoy programming, learning about different topics and teaching about maths and computer. 



🔍 Researches Interests:
 - Software Development
 - Statistical Modelling
 - Machine/Deep Learning
 - Cloud computing
 - Big Data
 - Time Series


## My Tech Stack

### Languages & Frameworks

![Python Badge](https://img.shields.io/badge/Python-100000?style=for-the-badge&logo=python&logoColor=darkgreen)
![JN Badge](https://img.shields.io/badge/Jupyter-100000.svg?&style=for-the-badge&logo=Jupyter&logoColor=whiten)
![R Badge](https://img.shields.io/badge/R-100000?style=for-the-badge&logo=r&logoColor=blue)
![C Badge](https://img.shields.io/badge/C-100000?style=for-the-badge&logo=c&logoColor=Yellow)

![Scala Badge](https://img.shields.io/badge/Scala-DC322F?style=for-the-badge&logo=scala&logoColor=white)
![FastApi Badge](https://img.shields.io/badge/fastapi-109989?style=for-the-badge&logo=FASTAPI&logoColor=white)
![Docker Badge](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)

![TF Badge](https://img.shields.io/badge/TensorFlow-FF6F00?style=for-the-badge&logo=TensorFlow&logoColor=white)
![Conda Badge](https://img.shields.io/badge/conda-342B029.svg?&style=for-the-badge&logo=anaconda&logoColor=white)

![MYSQL Badge](https://img.shields.io/badge/MySQL-2311AB00?style=for-the-badge&logo=mysql&logoColor=white)
![POST Badge](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white)
![NOSQL Badge](https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white)

### Cloud

![GCP Badge](https://img.shields.io/badge/Google_Cloud-100000?style=for-the-badge&logo=google-cloud&logoColor=darkgreen)
![AZURE Badge](https://img.shields.io/badge/microsoft%20azure-100000?style=for-the-badge&logo=microsoft-azure&logoColor=blue)
![AWS Badge](https://img.shields.io/badge/Amazon_AWS-100000?style=for-the-badge&logo=amazon-aws&logoColor=orange)

### Tools and Others

![Linux Badge](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)
![REDIS Badge](https://img.shields.io/badge/redis-%23DD0031.svg?&style=for-the-badge&logo=redis&logoColor=white)
![Git](https://img.shields.io/badge/-Git-black?style=for-the-badge&logo=git)

![Pycharm Badge](https://img.shields.io/badge/pycharm-143?style=for-the-badge&logo=pycharm&logoColor=black&color=black&labelColor=green)
![VIM Badge](https://img.shields.io/badge/VIM-%2311AB00.svg?&style=for-the-badge&logo=vim&logoColor=white)



## Certifications

<table>
  <tr>
    <th><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Microsoft_logo.svg/480px-Microsoft_logo.svg.png" width="35" height="35" /> </th>
    <th >Microsoft Certified: Azure Data Engineer Associate</th>
  </tr>
</table>

<table>
  <tr>
    <th><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Microsoft_logo.svg/480px-Microsoft_logo.svg.png" width="35" height="35" /> </th>
    <th >Microsoft Certified: Azure Fundamentals</th>
  </tr>
</table>


<table>
  <tr>
    <th><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Coursera-Logo_600x600.svg/1200px-Coursera-Logo_600x600.svg.png" width="35" height="35" /> </th>
    <th >Machine Learning</th>
  </tr>
</table>

<table>
  <tr>
    <th><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Coursera-Logo_600x600.svg/1200px-Coursera-Logo_600x600.svg.png" width="35" height="35" /> </th>
    <th >Neural Networks and Deep Learning</th>
  </tr>
</table>

<table>
  <tr>
    <th><img src="https://i.pinimg.com/originals/d1/d7/11/d1d7113a292af6ebbe146a83c8a752a5.png" width="35" height="35" /> </th>
    <th >Docker Mastery: with Kubernetes +Swarm from a Docker Captain</th>
  </tr>
</table>

<table>
  <tr>
    <th><img src="https://i.pinimg.com/originals/d1/d7/11/d1d7113a292af6ebbe146a83c8a752a5.png" width="35" height="35" /> </th>
    <th >Taming Big Data with Apache Spark and Python - Hands On! </th>
  </tr>
</table>

